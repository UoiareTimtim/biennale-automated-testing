const {Builder, By, Key, Util} = require("selenium-webdriver");

let driverChrome = new Builder().forBrowser("chrome").build();
// let windowVar = wind

async function openChrome(){
    // await driverChrome.build().get("http://biennale:raedEqDKcqveyb@!2019@staging.singaporebiennale.org");
    
    await driverChrome.get("http://staging.singaporebiennale.org");
    
    try {
        driverChrome.sleep(5000);
        let Whatsonelement = driverChrome.findElement(By.xpath("//header[@id='mainHeader']//a[@href='/art']"));
        await driverChrome.actions().move({ duration: 5000, origin: Whatsonelement, x: 0, y: 0 }).perform();
        driverChrome.sleep(3000);
        driverChrome.executeScript(scrollDown);
        let BookmarkElemnt = driverChrome.findElement(By.xpath("//aside[@class='toolbar']//button[@class='tooltip tooltip--bookmark']"));
        let BookmarkElemnt1 = driverChrome.findElement(By.xpath("//aside[@class='toolbar']//i[@class='ico-bookmark anim-all']"));
        await driverChrome.actions().move({ duration: 5000, origin: BookmarkElemnt1, x: 0, y: 0 }).perform();
        driverChrome.quit();
    } catch (error) {
        console.log(error);
    }
    
    
}
 
function scrollDown(){window.scrollBy(0,200);};
    

openChrome();
